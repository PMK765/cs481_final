﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Final0
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class expandStockPage : ContentPage
	{
		public expandStockPage ()
		{
			InitializeComponent ();
		}

        public expandStockPage(TopStock stockData)
        {
            InitializeComponent();
            BindingContext = stockData;

        }

        async private void ContentPage_Appearing(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                Spinner.IsVisible = true;
                Spinner.IsRunning = true;
                var client = new HttpClient();
                var searchApiAddress = "http://webhose.io/filterWebContent?" +
                    "token=71c9a9e9-13e7-4969-89de-2c6b2728ebc7&" +
                    "size=20&" +
                    "format=json&" + "sort=crawled&" +
                    "q=language%3Aenglish%20thread.title%3A" +
                    StockName.Text;
                var uri = new Uri(searchApiAddress);
                SearchModel searchData = new SearchModel();
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var jasonContent = await response.Content.ReadAsStringAsync();
                    searchData = JsonConvert.DeserializeObject<SearchModel>(jasonContent);
                }
                Spinner.IsRunning = false;
                Spinner.IsVisible = false;
                StockListView.ItemsSource = searchData.Posts;
            }
            else
            {
                await DisplayAlert("No Internet Connection", "Please Enable Internet", "OK");
            }
        }

        private void expStockListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            Post itemTapped = (Post)listView.SelectedItem;
            var uri = new Uri(itemTapped.Url);
            Device.OpenUri(uri);
        }
    }
    
}
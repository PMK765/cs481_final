﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_Final0
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class expandNewsPage : ContentPage
	{
		public expandNewsPage ()
		{
			InitializeComponent ();
		}
        public expandNewsPage(Article newsData)
        {
            InitializeComponent();
            BindingContext = newsData;
        }
        private void Button_Clicked(object sender, EventArgs e)
        {
            var uri = new Uri(urlButton.Text);
            Device.OpenUri(uri);
        }
    }
}
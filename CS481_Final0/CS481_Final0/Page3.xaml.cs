﻿using Microcharts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CS481_Final0.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MicroEntry = Microcharts.Entry;
using SkiaSharp;
using System.Collections.ObjectModel;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Connectivity;

namespace CS481_Final0
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        //ObservableCollection<MicroEntry> entryList = ObservableCollection<MicroEntry>;
		public Page3 ()
		{
			InitializeComponent();
            ChangeLabel.IsVisible = false;
        }

        async private void Stock_Button_Clicked(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                Spinner.IsVisible = true;
                Spinner.IsRunning = true;
                ObservableCollection<MicroEntry> entryList = new ObservableCollection<MicroEntry>();
                ObservableCollection<StockHistoryModel> StockMonth = new ObservableCollection<StockHistoryModel>();
                StockQuoteModel stockQuoteData = new StockQuoteModel();

                var client2 = new HttpClient();

                var stock_address2 = "https://api.iextrading.com/1.0/stock/" + StockEntry.Text.ToLower() + "/quote";
                var uri = new Uri(stock_address2);
                var response2 = await client2.GetAsync(uri);
                if (response2.IsSuccessStatusCode)
                {
                    var jsonContent2 = await response2.Content.ReadAsStringAsync();
                    stockQuoteData = JsonConvert.DeserializeObject<StockQuoteModel>(jsonContent2);
                }

                Spinner.IsVisible = false;
                Spinner.IsRunning = false;

                Label_0.Text = stockQuoteData.companyName;
                Label_1.Text = stockQuoteData.primaryExchange;
                TickerSymbol.Text = stockQuoteData.symbol;
                CurrentPrice.Text = "Trading at: " + stockQuoteData.latestPrice.ToString();
                High.Text = "Today's High: " + stockQuoteData.high.ToString();
                Low.Text = "Today's Low: " + stockQuoteData.low.ToString();
                DayChange.Text = "Change Over the Year: " + stockQuoteData.ytdChange.ToString();


                stock_address2 = "https://api.iextrading.com/1.0/stock/" + StockEntry.Text.ToLower() + "/chart/1m";
                var uri2 = new Uri(stock_address2);
                response2 = await client2.GetAsync(uri2);
                if (response2.IsSuccessStatusCode)
                {
                    var jsonContent2 = await response2.Content.ReadAsStringAsync();
                    StockMonth = JsonConvert.DeserializeObject<ObservableCollection<StockHistoryModel>>(jsonContent2);
                }
                for (int i = 0; i < StockMonth.Count; i++)
                {
                    int colorChange = i % 9;
                    var newEntry = new MicroEntry((float)StockMonth[i].change);
                    /*{
                        Label = StockMonth[i].label,
                        Color = SKColor.Parse("#234"+colorChange+"32"),
                    };*/
                    entryList.Add(newEntry);
                }
                ChangeLabel.IsVisible = true;
                var lineChart = new LineChart() { Entries = entryList };
                Chart1.Chart = lineChart;
            }
            else
            {
                await DisplayAlert("No Internet Connection", "Please Enable Internet", "OK");
            }
        }
    }
}
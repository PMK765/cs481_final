﻿using System;
using System.Collections.Generic;

namespace CS481_Final0.Models
{
    public class TopStock
    {
        public string symbol { get; set; }
        public string companyName { get; set; }
        public string primaryExchange { get; set; }
        public string sector { get; set; }
        public string calculationPrice { get; set; }
        public double latestPrice { get; set; }
        public double open { get; set; }
        public object openTime { get; set; }
        public double close { get; set; }
        public object closeTime { get; set; }
        public double high { get; set; }
        public double low { get; set; }
        public double change { get; set; }
        public double changePercent { get; set; }
        public int avgTotalVolume { get; set; }
        public object marketCap { get; set; }
        public double? peRatio { get; set; }
        public double week52High { get; set; }
        public double week52Low { get; set; }
        public double ytdChange { get; set; }
    }

}
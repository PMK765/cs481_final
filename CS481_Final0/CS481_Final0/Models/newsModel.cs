﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_Final0.Models
{
    public class Source
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class Article
    {
        public Source Source { get; set; }
        public object Author { get; set; }
        public string Title { get; set; }
        public object Description { get; set; }
        public string Url { get; set; }
        public object UrlToImage { get; set; }
        public DateTime PublishedAt { get; set; }
        public object Content { get; set; }
    }

    public class NewsModel
    {
        public string Status { get; set; }
        public int TotalResults { get; set; }
        public List<Article> Articles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481_Final0.Models
{
    public class Facebook
    {
        public int Likes { get; set; }
        public int Comments { get; set; }
        public int Shares { get; set; }
    }

    public class Gplus
    {
        public int Shares { get; set; }
    }

    public class Pinterest
    {
        public int Shares { get; set; }
    }

    public class Linkedin
    {
        public int Shares { get; set; }
    }

    public class Stumbledupon
    {
        public int Shares { get; set; }
    }

    public class Vk
    {
        public int Shares { get; set; }
    }

    public class Social
    {
        public Facebook Facebook { get; set; }
        public Gplus Gplus { get; set; }
        public Pinterest Pinterest { get; set; }
        public Linkedin Linkedin { get; set; }
        public Stumbledupon Stumbledupon { get; set; }
        public Vk Vk { get; set; }
    }

    public class Thread
    {
        public string Uuid { get; set; }
        public string Url { get; set; }
        public string Site_full { get; set; }
        public string Site { get; set; }
        public string Site_section { get; set; }
        public List<object> Site_categories { get; set; }
        public string Section_title { get; set; }
        public string Title { get; set; }
        public string Title_full { get; set; }
        public DateTime Published { get; set; }
        public int Replies_count { get; set; }
        public int Participants_count { get; set; }
        public string Site_type { get; set; }
        public string Country { get; set; }
        public double Spam_score { get; set; }
        public string Main_image { get; set; }
        public int Performance_score { get; set; }
        public int? Domain_rank { get; set; }
        public Social Social { get; set; }
    }

    public class Entities
    {
        public List<object> Persons { get; set; }
        public List<object> Organizations { get; set; }
        public List<object> Locations { get; set; }
    }

    public class Post
    {
        public Thread Thread { get; set; }
        public string Uuid { get; set; }
        public string Url { get; set; }
        public int Ord_in_thread { get; set; }
        public string Author { get; set; }
        public DateTime Published { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string HighlightText { get; set; }
        public string HighlightTitle { get; set; }
        public string Language { get; set; }
        public List<object> External_links { get; set; }
        public Entities Entities { get; set; }
        public object Rating { get; set; }
        public DateTime Crawled { get; set; }
    }

    public class SearchModel
    {
        public List<Post> Posts { get; set; }
        public int TotalResults { get; set; }
        public int MoreResultsAvailable { get; set; }
        public string Next { get; set; }
        public int RequestsLeft { get; set; }
    }
}
